﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;

namespace YQL_LIB
{
    public class YQL_LIB
    {
        private string dir;
        private string query;

        public YQL_LIB(string dir)
        {
            this.dir = dir;
            if (!System.IO.Directory.Exists(dir + "\\yql_calls"))
            {
                System.IO.Directory.CreateDirectory(dir + "\\yql_calls");
            }
        }

        public void YQLStringRequest(string query)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("https://query.yahooapis.com/v1/public/yql?");
            stringBuilder.Append("q=" + HttpUtility.UrlEncode(query));
            stringBuilder.Append("&format=json");
            stringBuilder.Append("&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=");
            this.query = stringBuilder.ToString().Replace("select", "SELECT").Replace("from", "FROM")
                .Replace("where", "WHERE");

        }

        public string getQueryString()
        {
            return query;
        }

        public string getResults()
        {
            using (var webClient = new WebClient())
            {

                string info =  webClient.DownloadString(query);
                return info;
            }
        }

        public List<string> getDataFromResults(string dataType)
        {
            var jarray = (JArray) JObject.Parse(getResults())["query"]["results"]["Result"];
            var stringList = new List<string>();
            if (dataType.Contains(','))
            {
                var strArray = dataType.Split(',');
                foreach (var jtoken in jarray)
                {
                    var str1 = "";
                    foreach (var str2 in strArray)
                        if (jtoken != null && jtoken[str2] != null)
                            str1 = str1 + "\n - " + str2 + ": " + ((string) jtoken[str2]).Replace("-", "_");
                    stringList.Add(str1);
                }
            }
            else
            {
                foreach (var jtoken in jarray)
                    stringList.Add((string) jtoken[dataType]);
            }
            return stringList;
        }

        public string Find(string item, string location, string data, string index)
        {
            YQLStringRequest(
                "select * from local.search where query=\"" + item + "\" and location=\"" + location + "\"");
            var dataFromResults = getDataFromResults(data);
            if (dataFromResults.Count > Convert.ToInt32(index))
                return dataFromResults[Convert.ToInt32(index)];
            return "there are not that many places, select a smaller index number";
        }

        public List<string> getDataFromResults(string dataType, string third_datatype)
        {
            var jarray = (JArray) JObject.Parse(getResults())["query"]["results"][third_datatype];
            var stringList = new List<string>();
            foreach (var jtoken in jarray)
                stringList.Add((string) jtoken[dataType]);
            return stringList;
        }

        public string getElementFromResults(string element, int index)
        {
            return getDataFromResults(element)[index];
        }
    }
}